import pandas as pd
import plotly.express as px
import json
import boto3
import io
import codecs

def plot_data(event, context):

    client = boto3.client('lambda')
    response = client.invoke(FunctionName='arn:aws:lambda:us-east-1:037728188742:function:bcp-function-dev-get_data')
    response = response['Payload']
    response = json.load(response)
    data = pd.read_json(response['body'])

    Type = data.groupby('material_name').sum()

    ##Pie Chart
    fig1 = px.pie(Type, values='quantity', names= Type.index,# hoverlabel= None,
                    color_discrete_sequence=["#003366", "#006699","#009999","#70d1d1",
                "#7496D2","#9BB7D4", "#D3DDDC", "#F7EDD4",
                "#E8D2B9", "#E6C675","#F5DF4D"," #ffff00"],
                title="All items found (2017-2021)")

    fig1.update_traces(textinfo='percent', textfont_size=15)
    fig1.update_traces(hovertemplate=None)
    fig1.update_layout(legend=dict(font=dict( size=16)),
            title=dict(font=dict( size=20)))


    f = io.StringIO()
    fig1.write_html(f)
    f.seek(0)

    s3_client = boto3.client('s3', region_name="us-east-1")

    bio = io.BytesIO()
    StreamWriter = codecs.getwriter('utf-8')
    wrapper_file = StreamWriter(bio)
    print(f.read(), file=wrapper_file)
    bio.seek(0)

    response = s3_client.upload_fileobj(bio, "beachcoop-data", "plot1.html",ExtraArgs={'ContentType': "text/html", 'ACL':'public-read'})

    # f.seek(0)
    # bio.seek(0)

    # bar plot
    DD = data.groupby("itemname")["quantity"].sum()

    dat = DD.loc[["Chip Packets","Cooldrink Bottles","Cooldrink Lids", "Ear buds" ,
                    "Nylon Rope or Net pieces", "Individual Sweet Wrappers",
                "Lighters","Lightsticks","Lollipop Sticks","Straws","Water Bottles"]]

    dat = pd.DataFrame(dat)
    dat = dat.sort_values('quantity', ascending=True)

    fig2 = px.bar(dat,orientation='h', color_discrete_sequence=["#003366"],
                title="Total Dirty Dozen items found (2017-2021)",
                labels={
                        "itemname": "",
                        "value": "number of items"
                    }, width=700, height=500)
    fig2.update_traces(hovertemplate=None)

    fig2.update_layout(   
        showlegend=False,                    
        title=dict(font=dict( size=20)),
        yaxis = dict(tickfont = dict(size=16)), 
        xaxis = dict(tickfont = dict(size=16)),
        xaxis_title="No. of items", font=dict(size=16),
    )

    f = io.StringIO()
    fig2.write_html(f)
    f.seek(0)

    s3_client = boto3.client('s3', region_name="us-east-1")

    bio = io.BytesIO()
    StreamWriter = codecs.getwriter('utf-8')
    wrapper_file = StreamWriter(bio)
    print(f.read(), file=wrapper_file)
    bio.seek(0)
    response = s3_client.upload_fileobj(bio, "beachcoop-data", "plot2.html",
    ExtraArgs={'ContentType': "text/html", 'ACL':'public-read'})


    return {
        "event": response
    }

